import { Component, OnInit } from '@angular/core';

import { NewsService } from '../../services';

import { News } from '../../types';

@Component({
  selector: 'news-list',
  styleUrls: ['./list.component.scss'],
  templateUrl: './list.component.html'
})

export class NewsListComponent implements OnInit{
	public news: News[] = [];
	private keys = [];
	private error: String = '';

	constructor(
		private newsService: NewsService
	){}

	ngOnInit() {
		this.getNews();
	}

	getNews() {
		this.newsService.getNews().subscribe(
			news => {
				if(Object.keys(news).length) {
					this.news = news;
					this.keys = Object.keys(this.news);
				} else {
					this.error = "We did not receive news"
				}
				
			},
			error => console.error(error)
		);
	}

}
