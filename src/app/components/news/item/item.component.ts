import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../../../services';

import { News } from '../../../types';

@Component({
  selector: 'news-item',
  styleUrls: ['./item.component.scss'],
  templateUrl: './item.component.html'
})

export class NewsItemComponent implements OnInit {
	public news: News = {
		title: '',
  		img: '',
  		text: ''
	};
	private newsId: String = '';
	private error: String = '';

	constructor(
		private activatedRoute: ActivatedRoute,
		private newsService: NewsService
	) {}

	ngOnInit() {
		this.activatedRoute.params.subscribe(p => this.getNewsById(p && p['id']));
	}

	getNewsById(newsId) {
		this.newsService.getNewsById(newsId).subscribe(
			news => {
				if (Object.keys(news).length) {
					this.news = news;
				} else {
					this.error = "We did not receive news"
				}
				
			},
			error => console.error(error)
		)
	}

}
