import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var navigator;

@Component({
  selector: 'news-add',
  styleUrls: ['./add.component.scss'],
  templateUrl: './add.component.html'
})

export class NewsAddComponent implements OnInit{
	contactForm: FormGroup;

	constructor(
		private formBuilder: FormBuilder
	) {}

	ngOnInit() {
		this.contactForm = this.formBuilder.group({
			title: ['', Validators.required],
			img: ['', Validators.required],
			text: ['', Validators.required]
		});
	}

	submitForm(): void {
		if (this.contactForm.valid == false) {
			alert('Все поля должны быть заполнены');
		} else {
			let token = localStorage.getItem('token') || "";
			navigator.serviceWorker.controller.postMessage({type: 'sync', dataForm:this.contactForm.value, token: token});
			this.contactForm.reset();
			alert('Отправено!');
		}
	}
}