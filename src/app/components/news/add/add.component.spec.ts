import {
  inject,
  TestBed
} from '@angular/core/testing';

// Load the implementations that should be tested
import { NewsAddComponent } from './add.component';
import { FormBuilder } from '@angular/forms';

describe('Add News', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      NewsAddComponent,
      FormBuilder
    ]}));

  it('should create', inject([ NewsAddComponent ], (addNews: NewsAddComponent) => {
    expect(addNews).toBeTruthy();
  }));

});
