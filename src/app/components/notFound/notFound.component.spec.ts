import {
  inject,
  TestBed
} from '@angular/core/testing';

// Load the implementations that should be tested
import { NotFoundComponent } from './notFound.component';

describe('NotFound', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      NotFoundComponent
    ]}));

  it('should create', inject([ NotFoundComponent ], (notFound: NotFoundComponent) => {
    expect(notFound).toBeTruthy();
  }));

});
