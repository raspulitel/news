import {Component} from '@angular/core';

@Component({
  selector: 'notFound',
  styleUrls: ['./notFound.component.scss'],
  templateUrl: './notFound.component.html'
})
export class NotFoundComponent {
}
