import {
  inject,
  TestBed
} from '@angular/core/testing';

// Load the implementations that should be tested
import { UnauthorizedComponent } from './unauthorized.component';

describe('Unauthorized', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      UnauthorizedComponent
    ]}));

  it('should create', inject([ UnauthorizedComponent ], (unauthorized: UnauthorizedComponent) => {
    expect(unauthorized).toBeTruthy();
  }));

});
