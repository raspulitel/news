import {TestBed, inject, tick} from '@angular/core/testing';
import {NewsService} from './news.service';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {Response, ResponseOptions, Http, ConnectionBackend, BaseRequestOptions, RequestOptions} from '@angular/http';

describe('Service: News', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                providers: [
                    {provide: RequestOptions, useClass: BaseRequestOptions},
                    {provide: ConnectionBackend, useClass: MockBackend},
                    Http,
                    NewsService
                ]
            });
        });

        it('should inject the service', inject([NewsService], (service: NewsService) => {
            expect(service).toBeTruthy();
        }));

        it('should download all items from the backend', inject([ConnectionBackend, NewsService],
            (backend: MockBackend,
             service: NewsService) => {
            // Arrange
            let items = null;
            let newsData = {
                "0": {
                    "img": "http://www.segodnya.ua/img/article/10206/30_main_new.1494757451.jpg",
                    "text": "Вчера, 13 мая, в Киеве завершился 62-й песенный конкурс Евровидение, который для украинской рок-группы O. Torvald стал провалом, а вот 27-летний португалец Сальвадор Собрал собрал все почести, одержав победу.Хозяева конкурса, рокеры из O. Torvald поступили слишком гостеприимно, пропустив в финале почти всех гостей вперед, оказавшись 24-ми среди 26-ти участников. Однако стоит отметить, что несмотря на неудовлетворительное выступление украинских музыкантов, в соцсети нашлось немало тех, кого порадовало их выступление.",
                    "title": "Евровидение-2017: в соцсетях оценили провал O. Torvald и победу Португалии"
                },
                "1": {
                    "img": "http://www.5.ua/media/pictures/820x546/104566.jpg",
                    "text": "У День Європи під Адміністрацією президента поряд із українськими стягами замайоріли прапори ЄС. Петро Порошенко наголосив, що Україна – це єдина держава, яка не входить до складу Європейського Союзу, але на офіційному рівні святкує День Європи. Урочистості відбувалися під гімн Європи.Глава держави зазначив, що Україна відстоює всій вибір на двох фронтах. Перший – дипломатичний та культурний, як конкурс Євробачення, завдяки якому десятки тисяч європейців познайомилися з Україною. Другий – той, що відбувається на сході України, де українські військові захищають кордони країни від російської агресії. Президент наголосив, що за свій вибір українці змушені платити високу ціну. Петро Порошенко з сумом нагадав про вчорашній жорстокий обстріл проросійськими бойовиками мирного населення Авдіївки.",
                    "title": "З НАГОДИ ДНЯ ЄВРОПИ ПІД АДМІНІСТРАЦІЄЮ ПРЕЗИДЕНТА ЗАМАЙОРІВ ПРАПОР ЄС"
                },
                "2": {
                    "img": "http://ubr.ua/img/article/38423/60_main.jpeg",
                    "text": "Мэр столицы отметил, что в целом на шоу Евровидение было продано более 60 тысяч билетов.Кличко также напомнил, что за всю историю конкурса в Киеве был самый большой EvroVillage, который каждый вечер принимал 350 тысяч человек.",
                    "title": "Кличко назвал количество посетивших Евровидение гостей"
                }
            };


            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/news.json?auth=');
                c.mockRespond(new Response(new ResponseOptions({body: newsData})));
            });

            // Act
            service.getNews().subscribe((q) => {
                items = q;
            });

            // Assert
            backend.verifyNoPendingRequests();
            expect(items).toEqual(newsData);
        }));

        it('should download one item from the backend', inject([ConnectionBackend, NewsService],
            (backend: MockBackend,
             service: NewsService) => {
            // Arrange
            let items = null;
            let newsId = 0;
            let newsData = {
              "img": "http://www.segodnya.ua/img/article/10206/30_main_new.1494757451.jpg",
              "text": "Вчера, 13 мая, в Киеве завершился 62-й песенный конкурс Евровидение, который для украинской рок-группы O. Torvald стал провалом, а вот 27-летний португалец Сальвадор Собрал собрал все почести, одержав победу.Хозяева конкурса, рокеры из O. Torvald поступили слишком гостеприимно, пропустив в финале почти всех гостей вперед, оказавшись 24-ми среди 26-ти участников. Однако стоит отметить, что несмотря на неудовлетворительное выступление украинских музыкантов, в соцсети нашлось немало тех, кого порадовало их выступление.",
              "title": "Евровидение-2017: в соцсетях оценили провал O. Torvald и победу Португалии"
            };

            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/news/' + newsId + '.json?auth=');
                c.mockRespond(new Response(new ResponseOptions({body: newsData})));
            });

            // Act
            service.getNewsById(newsId).subscribe((q) => {
                items = q;
            });

            // Assert
            backend.verifyNoPendingRequests();
            expect(items).toEqual(newsData);
        }));

        it('should log an error to the console on error when retrieve all news', inject([ConnectionBackend, NewsService],
            (backend: MockBackend,
             service: NewsService) => {

            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/news.json?auth=');
                c.mockRespond(new Response(new ResponseOptions(
                    {
                        body: { error: `I'm afraid I've got some bad news!` },
                        status: 500
                    }
                )));
            });

            spyOn(console, 'error');

            service.getNews().subscribe(null, () => {
                expect(console.error).toHaveBeenCalledWith(`I'm afraid I've got some bad news!`);
            });
        }));

        it('should log an error to the console on error when retrieve one news', inject([ConnectionBackend, NewsService],
            (backend: MockBackend,
             service: NewsService) => {

            let newsId = 0;
            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/news/' + newsId + '.json?auth=');
                c.mockRespond(new Response(new ResponseOptions(
                    {
                        body: { error: `I'm afraid I've got some bad news!` },
                        status: 500
                    }
                )));
            });

            spyOn(console, 'error');

            service.getNewsById(newsId).subscribe(null, () => {
                expect(console.error).toHaveBeenCalledWith(`I'm afraid I've got some bad news!`);
            });
        }));
    });