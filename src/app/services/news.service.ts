import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class NewsService {
    private token: string;

    constructor(private http: Http) {
        this.token = localStorage.getItem('token') || "";
    }

    private get(url: string) {
        return this.http.get(url).catch(this.handleError.bind(this));
    }

    getNews(){
        return this.get(`https://news-71c66.firebaseio.com/news.json?auth=${this.token}`)
            .map(this.extractData)        
    }

    getNewsById(newsId){
        return this.get(`https://news-71c66.firebaseio.com/news/` + newsId + `.json?auth=${this.token}`)
            .map(this.extractData)        
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
}
