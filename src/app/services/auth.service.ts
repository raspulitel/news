import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
    private requestOptions: RequestOptions;
    private headers: Headers;

    constructor(private http: Http) {
        this.setHeaders();
    };

    private setHeaders() {
        const accessToken: String = localStorage.getItem('token') || "";

        this.headers = new Headers({'x-access-token': accessToken});
        this.requestOptions = new RequestOptions({headers: this.headers});
    }

    private get(url) {
        return this.http.get(url, {
            headers: this.headers
        }).catch(this.handleError.bind(this));
    }

    authApp(password) {
        return this.get(
                'https://news-71c66.firebaseio.com/users.json?orderBy="password"&startAt="' + password + '"'
            ).map(this.extractData)   
    }    

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return Observable.throw(errMsg);
    }
}
