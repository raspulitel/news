import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
      private router: Router
    ) {}

    canActivate() {
        const token: String = localStorage.getItem('token') || '';

        if (!token || token == '') {
            this.router.navigate(['/unauthorized']);
        }

        return true;
    }
}
