import {TestBed, inject, tick} from '@angular/core/testing';
import {AuthService} from './auth.service';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {Response, ResponseOptions, Http, ConnectionBackend, BaseRequestOptions, RequestOptions} from '@angular/http';

describe('Service: Auth', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                providers: [
                    {provide: RequestOptions, useClass: BaseRequestOptions},
                    {provide: ConnectionBackend, useClass: MockBackend},
                    Http,
                    AuthService
                ]
            });
        });

        it('should inject the service', inject([AuthService], (service: AuthService) => {
            expect(service).toBeTruthy();
        }));

        it('should download all items from the backend', inject([ConnectionBackend, AuthService],
            (backend: MockBackend,
             service: AuthService) => {
            // Arrange
            let items = null;
            let password = 123456;
            let authData = {
              "admin": {
                "password": "123456",
                "token": "supertoken"
              }
            };


            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/users.json?orderBy="password"&startAt="'+password+'"');
                c.mockRespond(new Response(new ResponseOptions({body: authData})));
            });

            // Act
            service.authApp(password).subscribe((q) => {
                items = q;
            });

            // Assert
            backend.verifyNoPendingRequests();
            expect(items).toEqual(authData);
        }));

        
        it('should log an error to the console on error when retrieve all news', inject([ConnectionBackend, AuthService],
            (backend: MockBackend,
             service: AuthService) => {

            let password = 123456;

            backend.connections.subscribe((c: MockConnection) => {
                expect(c.request.url).toEqual('https://news-71c66.firebaseio.com/users.json?orderBy="password"&startAt="'+password+'"');
                c.mockRespond(new Response(new ResponseOptions(
                    {
                        body: { error: `I'm afraid I've got some bad news!` },
                        status: 500
                    }
                )));
            });

            spyOn(console, 'error');

            service.authApp(password).subscribe(null, () => {
                expect(console.error).toHaveBeenCalledWith(`I'm afraid I've got some bad news!`);
            });
        }));
    });