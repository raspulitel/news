import { Routes } from '@angular/router';
import { 
	HomeComponent, 
	NewsListComponent, 
	NewsItemComponent, 
	NewsAddComponent,
	UnauthorizedComponent,
  NotFoundComponent
} from './components';

import { 
	AuthGuard, 
} from './services';


export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },

  { path: 'news', canActivate: [AuthGuard],
    children: [
      { path: '', component: NewsListComponent },
      { path: 'add', component: NewsAddComponent, pathMatch: 'full'},
      { path: ':id', component: NewsItemComponent },      
    ]
  },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: '**', component: NotFoundComponent }
];

