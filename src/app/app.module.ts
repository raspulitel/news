import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { 
  HomeComponent, 
  NewsListComponent, 
  NewsItemComponent, 
  NewsAddComponent,
  UnauthorizedComponent,
  NotFoundComponent
} from './components';

import { NewsService, AuthService, AuthGuard } from "./services";


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewsListComponent,
    NewsItemComponent,
    NewsAddComponent,
    UnauthorizedComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true })
  ],
  providers: [
    AuthService,
    NewsService,
    AuthGuard
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}
