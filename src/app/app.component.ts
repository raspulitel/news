import { Component, OnInit } from '@angular/core';
import { AuthService } from './services';

@Component({
	selector: 'app',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	private appSecret: String = 'news_admin_322';
	private error: String = '';

	constructor(
		private authService: AuthService
	) {}

	ngOnInit() {
		localStorage.clear();
		this.authApp();
	}

	authApp(): void {
		this.authService.authApp(this.appSecret).subscribe(
			authData => {
				if (Object.keys(authData).length) {
					localStorage.setItem("token", authData.admin.token);
				} else {
					alert('App don\'t authorized');
				}
				
			},
			error => console.error(error)
		)
	}

}
