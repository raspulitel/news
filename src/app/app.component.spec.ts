// import { async, ComponentFixture, TestBed
// } from '@angular/core/testing';

// import { DebugElement } from '@angular/core';
// import { By } from '@angular/platform-browser';

// import { NO_ERRORS_SCHEMA }          from '@angular/core';
// import { AppComponent }              from './app.component';
// import { RouterLinkStubDirective }   from '../testing';
// import { RouterOutletStubComponent } from '../testing';


// let comp:    AppComponent;
// let fixture: ComponentFixture<AppComponent>;

// //////// Testing w/ NO_ERRORS_SCHEMA //////
// describe('AppComponent & NO_ERRORS_SCHEMA', () => {
//   beforeEach( async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ AppComponent, RouterLinkStubDirective ],
//       schemas:      [ NO_ERRORS_SCHEMA ]
//     })

//     .compileComponents()
//     .then(() => {
//       fixture = TestBed.createComponent(AppComponent);
//       comp    = fixture.componentInstance;
//     });
//   }));
//   tests();
// });


// function tests() {
//   let links: RouterLinkStubDirective[];
//   let linkDes: DebugElement[];

//   beforeEach(() => {
//     // trigger initial data binding
//     fixture.detectChanges();

//     // find DebugElements with an attached RouterLinkStubDirective
//     linkDes = fixture.debugElement
//       .queryAll(By.directive(RouterLinkStubDirective));

//     // get the attached link directive instances using the DebugElement injectors
//     links = linkDes
//       .map(de => de.injector.get(RouterLinkStubDirective) as RouterLinkStubDirective);
//   });

//   it('can instantiate it', () => {
//     expect(comp).not.toBeNull();
//   });

//   it('can get RouterLinks from template', () => {
//     expect(links.length).toBe(3, 'should have 3 links');
//     expect(links[0].linkParams).toBe('/dashboard', '1st link should go to Dashboard');
//     expect(links[1].linkParams).toBe('/heroes', '1st link should go to Heroes');
//   });

//   it('can click Heroes link in template', () => {
//     const heroesLinkDe = linkDes[1];
//     const heroesLink = links[1];

//     expect(heroesLink.navigatedTo).toBeNull('link should not have navigated yet');

//     heroesLinkDe.triggerEventHandler('click', null);
//     fixture.detectChanges();

//     expect(heroesLink.navigatedTo).toBe('/heroes');
//   });
// }


// /*
// Copyright 2017 Google Inc. All Rights Reserved.
// Use of this source code is governed by an MIT-style license that
// can be found in the LICENSE file at http://angular.io/license
// */