const syncStore = {};
let token;

self.addEventListener('sync', function (event) {
	console.log('Sync');
 	const data = syncStore[event.tag]
  	event.waitUntil(sendNews(data))
});

self.addEventListener('message', event => {
	console.log('Get message');
	if(event.data.type === 'sync') {
		const id = guid()
		syncStore[id] = event.data.dataForm;
		token = event.data.token;
		self.registration.sync.register(id)
	}
})

function sendNews(data) {
	console.log('Send News');
	fetch("https://news-71c66.firebaseio.com/news.json?auth=" + token, {  
		method: 'post',  
		headers: {  
		"Content-type": "application/json"  
	},  
		body: JSON.stringify(data)
	})
	.then(function (data) {  
		console.log('Request succeeded with JSON response', data);  
	})  
	.catch(function (error) {  
		console.log('Request failed', error);  
	});
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}